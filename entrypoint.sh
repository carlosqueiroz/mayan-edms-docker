#!/bin/bash
set -e

INSTALL_FLAG=/var/lib/mayan/settings/local.py
MAYAN_INSTALL_DIR=/usr/local/lib/python2.7/dist-packages/mayan

initialize() {
    mayan-edms.py initialsetup
    # Copy the generated local.py with the SECRET KEY to the volume
    cp $MAYAN_INSTALL_DIR/settings/local.py $MAYAN_INSTALL_DIR/media/settings/local.py
    chown -R www-data:www-data $MAYAN_INSTALL_DIR
}

upgrade() {
    mayan-edms.py performupgrade
}

start() {
    rm -rf /var/run/supervisor.sock
    exec /usr/bin/supervisord -nc /etc/supervisor/supervisord.conf
}

if [ "$1" = 'mayan' ]; then
    # Check if this is a new install, otherwise try to upgrade the existing
    # installation on subsequent starts
    if [ ! -f $INSTALL_FLAG ]; then
       initialize
    else
       upgrade
    fi
    start
fi

exec "$@"
