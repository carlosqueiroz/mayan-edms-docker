Changelog
=========
2017-05-XX
- Allow changing the database backend
- Allow changing the Celery broker
- Allow changing the Celery results backend
- Add a dedicated image converter worker
- Implement single installation step
- Implement single volume solution
- Increase NGINX max file size to 500 MB
- Increase NGINX file transfer timeout to 600 seconds
- When overrided, the database settings will add the option to keep connections
  alive with a default of 60 seconds. ('CONN_MAX_AGE': 60)

New environment variables:

- MAYAN_DATABASE_DRIVER, default: None
- MAYAN_DATABASE_NAME, default : 'mayan'
- MAYAN_DATABASE_USER, default: 'mayan'
- MAYAN_DATABASE_PASSWORD, default: ''
- MAYAN_DATABASE_HOST, default: None
- MAYAN_DATABASE_PORT, default: None
- MAYAN_BROKER_URL, default: 'redis://127.0.0.1:6379/0'
- MAYAN_CELERY_RESULT_BACKEND, default: 'redis://127.0.0.1:6379/0'

- If the MAYAN_BROKER_URL and MAYAN_CELERY_RESULT_BACKEND are specified the built in
  REDIS server is disabled.
- Add health check.
- Add production and development Docker compose files.

2017-02-25
----------
- Update to Mayan EDMS 2.1.10.
- Add upgrade instructions.
- Add customization instructions.

2017-01-06
----------
- Add Vagrantfile to build and test Docker image.

2016-11-23
----------
- Update to Mayan EDMS version 2.1.6.
- Add HISTORY.md file.

