ALLOWED_HOSTS = ['*']

try:
    from mayan.media.settings.local import *  # NOQA
except ImportError:
    from .docker import *  # NOQA
